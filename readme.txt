Plugin Name:Products Slider for WP E-Commerce
Plugin URI: http://getshopped.org
Description: A plugin to Display a slider, or gallery of product images.
Author: Instinct Entertainment
Author URI: http://getshopped.org
Developer: Roy Ho, sasky
Developer URI: http://1plusdesign.com, sasky.org
Version: 3.0
License: Commercial

Thank you for purchasing our Wordpress e-Commerce Products Slider Plugin.

==FEATURES==
2 themes -- apple and Wordpress (apple uses a scroll bar and Wordpress uses next and previous buttons)

For Wordpress theme:
Ability to choose how many products visible on the slider.
Ability to choose how many products to slide by.
Ability to display price.

For Apple theme:
Ability to display price.


Both theme’s can display categories or products within a category

==INSTALLATION & SETUP==

Upload and install the plugin like you would any other plugin. The easiest way to do this is to open your Wordpress admin
dashboard and navigate to Plugins -> add new -> upload. Then upload the plugin in zip format. Proceed to activate the plugin.

The Product Slider Setting Page will be added in the  “Products -> - Product Slider”  menu.

==HOW TO USE==

There are two ways to use this plugin.  Shortcodes and Template tags.
Shortcodes can be placed into a post or pages content area to display a slider.
A template tag is a php function which you can place into one of your theme template files to display the slider

Shortcode:

Place this short code into a post or pages content area to render the Product slider.
[wpec_product_slider]

Shortcode options:

To alter how many products are shown in the product slider, you can add this
[wpec_product_slider number_of_images=”20”]
To display all of your products in your shop, place in -1, like so
[wpec_product_slider number_of_images=”-1”]
note  : If you have a large product catalog, then displaying all products is not advised as it could slow down the loading of
your page.
The default setting is to display ten products, in order of most recently added.

To Display particular product images, you can explicitly list them by product id, like so
[wpec_product_slider products=”5,9,12,15,21,30”  ]
To find what your product id’s are, go to your products list page in the WP-admin Dashboard ( Products -> Products )
Now hover over a products title, you should see a link displayed by your browser, usually at the bottom left  of your browser's window. It will look like this
( your sites url )/wp-admin/post.php?post=21&action=edit
Pay attention to what the post equals, like so “post=21”, that’s the product id of that product, in this case 21.

To Display Products in a particular Category, you can do that by the slug name of that category. Like so.
[wpec_product_slider category=”pants” ]
note : the category slug may be different from the category title, you can find the slugs of your categories, from the category table on the Products -> Categories page.

These options can be combined if it makes sense to do so, for example, this will display the 6 latest products from the category with a slug of ‘bikes’
[wpec_product_slider number_of_images=”6” category=”bikes” ]


Template Tag:
The Product slider can be rendered from your template files by using the following template tag.
<?php if(function_exists('wpec_product_slider')) wpec_product_slider(); ?>

The template tag can be customised in a similar way that the short code can, for example, for number of images  of 6 would be
<?php if(function_exists('wpec_product_slider')) wpec_product_slider(array('number_of_images' => 6)); ?>

For product id’s of 5,9,12,15,21,30 would be
<?php if(function_exists('wpec_product_slider')) wpec_product_slider(array('products' => '5,9,12,15,21,30')); ?>

For Category slug of bikes, it would be
<?php if(function_exists('wpec_product_slider')) wpec_product_slider(array('category' => 'bikes')); ?>

And to combine number of images as 6 and from a category slug of bikes, to would be
<?php if(function_exists('wpec_product_slider')) wpec_product_slider(array('number_of_images' => 6, 'category' => 'bikes')); ?>



==TIPS==

For the Wordpress theme, the slider will adapt to the amount of products you enable as well as the size of the image.

For the Apple theme, it is a set width of 960px for the container.  This is not resizeable.  The "visible" and "product scroll
by" settings do not affect this theme.