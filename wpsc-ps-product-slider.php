<?php
/*
Plugin Name:Products Slider for WP E-Commerce
Plugin URI: http://getshopped.org
Description: A plugin that provides a slider for product list
Author: Instinct
Author URI: http://getshopped.org
Developer: Roy Ho, sasky
Developer URI: http://1plusdesign.com, http://sasky.org
Version: 3.0
License: Commericial
*/
// define gloabal constants

define('WPSC_PS_VERSION' , '3.0');
define('WPSC_PS_MAIN_OPTION','wpec_product_slider');

// require all nessery files

require_once plugin_dir_path( __FILE__ ) . '/utils/Class-WPSC-PS-form-helper.php';
require_once plugin_dir_path( __FILE__ ) . '/admin/Class-WPSC-PS-slider-admin-page.php';
//require_once plugin_dir_path( __FILE__ ) . '/admin/Class-WPSC-PS-customizer.php';
require_once plugin_dir_path( __FILE__ ) . '/front/Class-WPSC-PS-product-object.php';
require_once plugin_dir_path( __FILE__ ) . '/front/Class-WPSC-PS-Theme-helpers.php';
require_once plugin_dir_path( __FILE__ ) . '/front/themes/wordpress/Class-WPSC-PS-wordpress-theme.php';
require_once plugin_dir_path( __FILE__ ) . '/front/themes/apple/Class-WPSC-PS-apple-theme.php';


class WPSC_product_slider{
    // in this class use get_theme() to return the theme object, dont call this var directly
    private $theme = null;

    public function __construct(){

        register_activation_hook( __FILE__, array( &$this,'register'));
        register_deactivation_hook( __FILE__,array( &$this,'un_register'));

        add_option('wpec_product_slider_path', plugin_dir_path( __FILE__ ));

        if(is_admin()){
            add_action( 'admin_menu', array(&$this,'admin_init'));
            // coming next version
             // add_action( 'customize_register', array( $this, 'customize_register' ) );
        } else {
            add_action('wp_enqueue_scripts', array( &$this,'load_css_and_js') );
            add_shortcode('wpec_product_slider',array (&$this, 'shortcode_hook') );
        }
    }

    public function register(){
        // runs when the plugin is activated
        // add the main option
        $defaults = array(
                    'display_type' => 'product',
                    'theme' => 'Wpsc_ps_Wordpress_theme',
                    'display_price' => 0 );

        $defaults = apply_filters('wpsc-ps-default-option', $defaults);
        update_option( WPSC_PS_MAIN_OPTION, $defaults );
    }

    public function un_register(){
        // runs when plugin is deactivated
        delete_option( WPSC_PS_MAIN_OPTION );
     }

    public function admin_init(){
        // Stating point for coding in the admin section
        new Wpsc_ps_admin_page();
    }
    /*
     * Coming next version
    public function customize_register( $wp_customize ){
        new Wpsc_ps_customizer( $wp_customize );
    }
    */
    public function load_css_and_js(){

        $this->get_theme()->load_css();
        $this->get_theme()->load_scripts();
    }

    public function shortcode_hook( $args ){

        $option = get_option(WPSC_PS_MAIN_OPTION);

        if( $option['display_type'] == 'product')
            $product_slider_data = $this->products_query( $args );
        elseif ( $option['display_type'] == 'cat')
            $product_slider_data = $this->category_query();

        ob_start();
        $this->get_theme()->render( $product_slider_data );
        $html = ob_get_contents();
        ob_end_clean();

        return $html;
    }
    private function products_query( $args ){
        $query_args = array(
            'post_type' => 'wpsc-product'
        );
       if( is_array( $args )){
           if(array_key_exists( 'number_of_images', $args )){
               $query_args['posts_per_page'] =  $args['number_of_images'];
           }
           if(array_key_exists( 'products', $args )){
               $query_args['post__in'] = explode(',',(trim(str_replace(' ', '', $args['products']))));
           }
           if(array_key_exists( 'category', $args )){
               $query_args['tax_query'] = array( array(
                                           'taxonomy' => 'wpsc_product_category',
                                           'field' => 'slug',
                                           'terms' => $args['category']));

           }

       }

       $theme_data =  $this->get_theme()->get_theme_data();
       $ps_Query = new WP_Query( $query_args );
       $product_slider_data = array();

       while ( $ps_Query->have_posts() ){
           $ps_Query->next_post();
           // checks if the product has an image, if not, don't include it in the product slider
           $thumb_nail =  wpsc_the_product_thumbnail($theme_data['width'],$theme_data['height'],$ps_Query->post->ID);


           if($thumb_nail){
               $product_slider_data[] = new WPSC_PS_Product_object(
                   $ps_Query->post->ID,
                   get_permalink($ps_Query->post->ID),
                   $thumb_nail,
                   $theme_data['width'],
                   $theme_data['height'],
                   $ps_Query->post->post_title,
                   wpsc_the_product_price( false, false,$ps_Query->post->ID));
           }
       }
       wp_reset_postdata();
       return $product_slider_data;

    }

    private function category_query(){
        $theme_data =  $this->get_theme()->get_theme_data();
        $terms = get_terms('wpsc_product_category');
        $product_slider_data = array();

        foreach($terms as $term){
            $id = (int) $term->term_id;
            $product_slider_data[] = new WPSC_PS_Product_object(
                $id,
                get_term_link( $id , 'wpsc_product_category') ,
                wpsc_category_image( $id ) ,
                $theme_data['width'] ,
                $theme_data['height'] ,
                wpsc_category_name( $id ) ,
                0 );

        }
        return $product_slider_data;
    }

    private function get_theme(){
        if( is_object($this->theme) ){
            return $this->theme;

        } else {
            $option = get_option(WPSC_PS_MAIN_OPTION);
            try {
                $this->theme = new $option['theme'];
            }
            catch ( Exception $e ){
                return new WP_Error($e, 'Try deactivating and then reactivating the WPEC Product Slider Plugin to fix this error');
            }

            return $this->theme;
         }
    }
}

$wpsc_ps_product_slider = new WPSC_product_slider();
/**
 * The Product slider template tag function
 *
 * The Product slider can be rendered from your template files by using the following template tag.
 * <?php if(function_exists('wpec_product_slider')) wpec_product_slider(); ?>
 *
 * The template tag can be customised in a similar way that the short code can, for example, for number of images  of 6 would be
 * <?php if(function_exists('wpec_product_slider')) wpec_product_slider(array('number_of_images' => 6)); ?>
 *
 * For product id’s of 5,9,12,15,21,30 would be
 * <?php if(function_exists('wpec_product_slider')) wpec_product_slider(array('products' => '5,9,12,15,21,30')); ?>
 *
 * For Category slug of bikes, it would be
 * <?php if(function_exists('wpec_product_slider')) wpec_product_slider(array('category' => 'bikes')); ?>
 *
 * And to combine number of images as 6 and from a category slug of bikes, to would be
 * <?php if(function_exists('wpec_product_slider')) wpec_product_slider(array('number_of_images' => 6, 'category' => 'bikes')); ?>
 *
 * @param Array $args the options for modifing the query for the slider. See readme for more info
**/

function wpec_product_slider( $args = null ){
    global $wpsc_ps_product_slider;
    echo $wpsc_ps_product_slider->shortcode_hook( $args );
}