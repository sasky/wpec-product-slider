<?php
Class WPSC_Form_helper {

    public static function radio_set( $name, $list, $selected ){

        if($list == 'bool'){
            $list = array( array( 'value' => '0', 'title' => __( 'No','wpsc')),
                           array( 'value' => '1', 'title' => __( 'Yes', 'wpsc')));
        }
        $html = '<ul>';
        foreach( $list as $list_item ){
            $checked = checked( $list_item['value'], $selected, false);
            $html .= '<li><input type="radio"  name="' . $name . '" value="' . $list_item['value'] . '" ' . $checked .'>    '. $list_item['title'] .'</li>';
        }
        $html .= '</ul>';
        echo $html;

    }
    public static function drop_down_set( $name, $list, $selected ){

        $html =  '<select name="' . $name . '" style="width:100px" >';

        foreach( $list as $list_item ){
            $select = selected( $list_item, $selected, false);

            $html .= '<option value="' . $list_item . '" ' . $select . '>' . $list_item . '</option>';
        }
        $html .= '</select>';
        echo $html;
    }
    public static function number_input( $name, $value = '', $max_length = 3 ){

        $html = '<input type="text" name="' . $name . '" size="' . $max_length . '" maxlength=" ' . $max_length. '" value="' . $value . '" />';
        echo $html;
    }
}