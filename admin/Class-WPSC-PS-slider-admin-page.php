<?php
class Wpsc_ps_admin_page{
    // constants
    const SETTINGS_PAGE          = 'wpsc_product_slider';
    const SETTINGS_SECTION_MAIN  = 'product_slider_section';

    // vars
    private $option;
    private $option_list;
    private $theme_list;
    //
    public function __construct(){
        $this->option = get_option( WPSC_PS_MAIN_OPTION);

        $this->option_list = array(

            'display_type'  => array( array( 'value' => 'product', 'title' => __('Product','wpsc')),
                                      array( 'value' => 'cat'    , 'title' => __('Category', 'wpsc'))),
            'display_price' => 'bool',

        );
        $this->theme_list = array();
        $this->theme_list = apply_filters('wpsc_ps_add_theme_to_menu', $this->theme_list );

        add_filter( 'wpsc_additional_pages', array( &$this, 'hook_admin_page' ) , 10, 2 );
        add_action( 'admin_init'     , array($this , 'admin_settings'));
    }
    public function hook_admin_page( $page_hooks, $base_page ) {
        //Add admin Module page
		$page_hooks[] =  add_submenu_page(  $base_page,
                                            __( '- Product Slider', 'wpsc' ),
		                                    __( '- Product Slider', 'wpsc' ),
		                                    'administrator',
                                            self::SETTINGS_PAGE,
		                                    array(&$this,'admin_page' ));
		return $page_hooks;
	}
    public function admin_settings(){
        register_setting( WPSC_PS_MAIN_OPTION, WPSC_PS_MAIN_OPTION, array( &$this, 'save' ) );
         add_settings_section(   self::SETTINGS_SECTION_MAIN,
                                 __( 'WP e-Commerce Product Slider Settings', 'wpsc' ),
                                 array( &$this, 'section_html'),
                                 self::SETTINGS_PAGE);

         add_settings_field( 'field1',__( 'Type', 'wpsc'), array( &$this, 'field_1'), self::SETTINGS_PAGE, self::SETTINGS_SECTION_MAIN);
         add_settings_field( 'field2',__( 'Display Price', 'wpsc'), array( &$this, 'field_2'), self::SETTINGS_PAGE, self::SETTINGS_SECTION_MAIN);
         add_settings_field( 'field3',__( 'Theme', 'wpsc'), array( &$this, 'field_3'), self::SETTINGS_PAGE, self::SETTINGS_SECTION_MAIN);

        do_action('wpsc-ps-admin-page-form');
    }
    public function admin_page(){?>
        <script type="text/javascript" >
            /*
          jQuery(document).ready(function(){
              var wp = jQuery('#wpsc_product_slider .form-table:eq(1), #wpsc_product_slider .wp');
              var apple = jQuery('#wpsc_product_slider .form-table:eq(2), #wpsc_product_slider .apple');
              var gs = jQuery('#wpsc_product_slider .form-table:eq(3), #wpsc_product_slider .gs');
              var showhide = function(){
                  if(jQuery(this).is(':checked')){
                       if(jQuery(this).val() == 'wp'){
                           wp.show();
                           apple.hide();
                           gs.hide();
                       } else if (jQuery(this).val() == 'apple'){
                           wp.hide();
                           apple.show();
                           gs.hide();
                       } else if (jQuery(this).val() == 'gs'){
                           wp.hide();
                           apple.hide();
                           gs.show();
                       }
                   }
              }
              var input = jQuery("#wpsc_product_slider input[name='wpec_product_slider[theme]']");
              input.each(showhide);
              input.change(showhide);
          });
          */
          </script>
        <div class="wrap" id="wpsc_product_slider">
            <form action="options.php" method="post"><?php
               settings_fields( WPSC_PS_MAIN_OPTION );
               do_settings_sections( self::SETTINGS_PAGE ); ?>
               <br/>
               <input name="Submit" type="submit" value="Save Changes" />
            </form>
        </div>
        <?php
   }
   public function section_html(){
       echo '<div class="misc-pub-section"></div>';
       echo '<h4>General Settings</h4>';
   }
   public function field_1(){
       WPSC_Form_helper::radio_set(WPSC_PS_MAIN_OPTION . '[display_type]', $this->option_list['display_type'], $this->option['display_type']);
       echo '<p class="install-help" > Display Products or Categories. For Categories to work you will need to upload images for your  categories. This can be done in the &#x201c;Products-&gt;Categories&#x201d; menu, then click into a Category from the list. Images can be uploaded under the &quot;Advanced settings&quot; heading. For best results, don\'t upload an image larger than 200 x 200px</p>';
   }
   public function field_2(){
       WPSC_Form_helper::radio_set(WPSC_PS_MAIN_OPTION . '[display_price]', $this->option_list['display_price'], $this->option['display_price']);
       echo '<p class="install-help" >Display price under product image. (This setting does not affect Category display type)</P>';
   }
   public function field_3(){
       WPSC_Form_helper::radio_set(WPSC_PS_MAIN_OPTION . '[theme]', $this->theme_list , $this->option['theme']);
       echo '<p class="install-help" >Choose a theme for the Product Slider. WordPress is recommended </p>';
   }


   public function save( $option ){

       $valid = array();
       // find the image-width and image-height keys and validate them
       foreach($option as $key => $value ){
           $valid[$key] = $value;
           if( is_array( $value ) ){
               foreach($value as $key2 => $value2 ){
                    if ( $key2 == 'image-width' || $key2 == 'image-height'){
                      if( preg_match( '/[^0-9]+/', $value2 )) // if its not a number, set it to the default size
                           $valid[$key][$key2] = 96;
                       elseif( $value2 < 50 )
                           $valid[$key][$key2] = 50;
                        elseif( $value2 > 800)
                           $valid[$key][$key2] = 800;

                   }
               }
           }
        }
       return $valid;
   }

}
