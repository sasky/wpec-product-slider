<?php


interface I_wpsc_ps_Theme {

    public function load_css();
    public function load_scripts();
    public function get_theme_data( );
    public function render( $product_slider_data );
}


interface I_wpsc_ps_Admin {
    public function __construct();
    public function set_default_options( $defaults );
    public function add_to_menu( $theme_list );
    public function admin_form();
    public function admin_save();
}

class Wpsc_ps_Unquie_id {
    // lets see if this works, if not use a transient
    static private $id = 0;
    static public function new_id(){
        self::$id++;
    }
    static public function get_id(){
        return self::$id;
    }
}