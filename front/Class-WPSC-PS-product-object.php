<?php
class WPSC_PS_Product_object{
    private  $post_id = 0;
    private $permalink = '';
    private $image_url = '';
    private $image_width = 0;
    private $image_height = 0;
    private $title = '';
    private $price ='';

    public function __construct( $post_id,
                                 $permalink,
                                 $image_url,
                                 $image_width,
                                 $image_height,
                                 $title,
                                 $price){

        $this->post_id          = $post_id;
        $this->permalink        = $permalink;
        $this->image_url        = $image_url;
        $this->image_width      = $image_width;
        $this->image_height     = $image_height;
        $this->title            = $title;
        $this->price            = $price;


    }


    public function get_image_height(){
        return $this->image_height;
    }

    public function get_image_url(){
        return $this->image_url;
    }

    public function get_image_width(){
        return $this->image_width;
    }

    public function get_permalink(){
        return $this->permalink;
    }

    public function get_post_id(){
        return $this->post_id;
    }

    public function get_price(){
        return $this->price;
    }

    public function get_title(){
        return $this->title;
    }
}