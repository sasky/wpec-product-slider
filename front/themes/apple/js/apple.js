jQuery(document).ready(function(){
    var container = jQuery('div.sliderGallery'),
     ul = jQuery('ul', container),
     size = jQuery('div.sliderGallery ul li').size(),
     ui_width = jQuery('div.sliderGallery ul li').outerWidth(true),
     offset = 900,
     masterWidth = (size * ui_width) - offset;

    container.css('overflow', 'hidden');  // move auto to noscript style to avoid flicker

    jQuery('.slider', container).slider({
        min: 0,
        max: masterWidth,
        handle: '.ui-slider-handle',
        stop: function (event, ui) {
            ul.animate({'left' : ui.value * -1}, 500);
        },
        slide: function (event, ui) {
            ul.css('left', ui.value * -1);
        }
    });

   jQuery(".btn-left").click(function(){
       var elValue = jQuery('.slider', container).slider('option', 'value');
       if(elValue > 0) {
          elValue = elValue - masterWidth;
          if(elValue < 0) {
              elValue = 0;
          }
          jQuery(".sliderGallery .slider").slider('value', elValue);
          jQuery(".sliderGallery ul").animate({'left' : elValue * -1}, 500);
       }
    });
    jQuery(".btn-right").click(function(){
        var elValue = jQuery('.slider', container).slider('option', 'value');

          if(elValue < masterWidth) {
              elValue = elValue + masterWidth;
              if(elValue > masterWidth) {
                  elValue = masterWidth;
              }
              jQuery(".sliderGallery .slider").slider('value', elValue);
              jQuery(".sliderGallery ul").animate({'left' : elValue * -1}, 500);
          }
    });
    jQuery("span.end").click(function(){
        var elValue = jQuery('.slider', container).slider('option', 'value');
        elValue = masterWidth;
        jQuery(".sliderGallery .slider").slider('value', elValue);
        jQuery(".sliderGallery ul").animate({'left' : elValue * -1}, 500);
    });
    jQuery("span.start").click(function(){
      var elValue = jQuery('.slider', container).slider('option', 'value');
      elValue = 0;
      jQuery(".sliderGallery .slider").slider('value', elValue);
      jQuery(".sliderGallery ul").animate({'left' : elValue * -1}, 500);
    });


	jQuery('a.add_product').click( function() {
		jQuery("#dragdrop_spinner_slider").css('display', 'block');
		jQuery(this).attr('href', 'javascript:void(0);');
		var form_values = jQuery("form", jQuery(this).parent()).serialize();

		jQuery.post( 'index.php?ajax=true&user=true&drag_and_drop_cart=true', form_values, function(returned_data) {
																									
			eval(returned_data);
			jQuery("#dragdrop_spinner_slider").css('display', 'none');
			if(jQuery("#fancy_notification") != null) {
				jQuery("#loading_animation").css('display', 'none');
				//jQuery('#fancy_notificationimage').css("display", 'none');
				
			}
			//submitform(document.getElementById(form_id));
			
			return false;
			
		});
	});	

}); //close doc ready


