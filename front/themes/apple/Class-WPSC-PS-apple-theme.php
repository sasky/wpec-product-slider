<?php
class Wpsc_ps_Apple_admin implements I_wpsc_ps_Admin {
    const SETTINGS_SECTION_APPLE = 'product_slider_section_a';
    private $option_list = array();
    private $option = array();

    public function __construct(){
        add_action('wpsc-ps-admin-page-form', array( &$this, 'admin_form'));
        add_filter('wpsc-ps-default-option',  array( &$this, 'set_default_options'));
        add_filter('wpsc_ps_add_theme_to_menu', array( &$this, 'add_to_menu'));
    }
    public function set_default_options( $defaults ){
        $defaults['apple_options'] = array(  'image-width' => 96,
                                             'image-height'=> 96 );
        return $defaults;
    }
    public function add_to_menu( $theme_list ){
        $theme_list[] = array( 'value' => 'Wpsc_ps_Apple_theme', 'title' => __('Apple', 'wpsc'));
        return $theme_list;
    }
    public function admin_form(){
        $this->option = get_option( WPSC_PS_MAIN_OPTION );
        add_settings_section(   self::SETTINGS_SECTION_APPLE,
                                __( '', 'wpsc' ),
                                array( &$this, 'apple_html'),
                                Wpsc_ps_admin_page::SETTINGS_PAGE);

        add_settings_field( 'apple_field_image_width',__( 'Image Width', 'wpsc'), array( &$this, 'apple_field_image_width'), Wpsc_ps_admin_page::SETTINGS_PAGE, self::SETTINGS_SECTION_APPLE);
        add_settings_field( 'apple_field_image_height',__( 'Image Height', 'wpsc'), array( &$this, 'apple_field_image_height'), Wpsc_ps_admin_page::SETTINGS_PAGE, self::SETTINGS_SECTION_APPLE);
    }

    public function apple_html(){
        echo '<div class="misc-pub-section apple"></div>';
        echo '<h4 class="apple">Apple Theme Settings</h4>';
    }
    public function apple_field_image_width(){
        WPSC_Form_helper::number_input(WPSC_PS_MAIN_OPTION . '[apple_options][image-width]',$this->option['apple_options']['image-width'],3);
        echo '<p class="install-help" >The width of the images in pixels. Enter a value between 50 and 800</p>';
    }
    public function apple_field_image_height(){
        WPSC_Form_helper::number_input(WPSC_PS_MAIN_OPTION . '[apple_options][image-height]',$this->option['apple_options']['image-height'],3);
        echo '<p class="install-help" >The height of the images in pixels. Enter a value between 50 and 800</p>';
    }
    public function admin_save(){

    }

}
new Wpsc_ps_Apple_admin();
Class Wpsc_ps_Apple_theme implements I_wpsc_ps_Theme {
    public function admin_form(){

    }
    public function admin_save(){

    }
    public function load_css(){
        wp_enqueue_style('ps-wordpress', plugins_url('css/wpsc-ps-apple.css', __FILE__ ));
    }
    public function load_scripts(){
        wp_enqueue_script('jquery-ui-slider', plugins_url('/js/jquery-ui-slider.js' , __FILE__ ) , array('jquery', 'livequery'));
        wp_enqueue_script('product_slider_apple', plugins_url('/js/apple.js' , __FILE__ ), array('jquery', 'livequery','jquery-ui-slider'));
    }
    public function get_theme_data(){

        $theme_data = array( 'theme'=> '', 'width' => 0, 'height' => 0);
        $master_option = get_option(WPSC_PS_MAIN_OPTION);

        $theme_data['theme'] = 'apple';
        $theme_data['width'] = $master_option['apple_options']['image-width'];
        $theme_data['height'] = $master_option['apple_options']['image-height'];

        return $theme_data;
    }
    public function render( $product_slider_data ){
        Wpsc_ps_Unquie_id::new_id();
        $id = Wpsc_ps_Unquie_id::get_id();
        $main_option = get_option(WPSC_PS_MAIN_OPTION);?>
        <div id="sliderWrap">
            <div class="sliderGallery group">
            <img src="<?php echo plugins_url('img/drop_spinner.gif',__FILE__) ?>" alt="Load Spinner" id="dragdrop_spinner_slider" />
            <ul>
            <?php
            foreach( $product_slider_data as $ps_object):

                if ( strlen($ps_object->get_title()) > 30 )
                    $title  = substr( $ps_object->get_title(), 0, 10) . '...';
                else
                    $title = $ps_object->get_title();
                ?>
                <li>
                    <a href="<?php echo $ps_object->get_permalink(); ?>" >
                        <img class="item product_image" id="product_image_<?php echo $ps_object->get_post_id(); ?>" alt="<?php echo $title ?>" title="<?php echo $title ?>" src="<?php echo $ps_object->get_image_url(); ?>" width="<?php echo $ps_object->get_image_width(); ?>" height="<?php echo $ps_object->get_image_height(); ?>" />
                    </a>
                    <?php if( $main_option['display_type'] == 'product' ) : ?>
                    <a href="javascript:void(0);" class="add_product" >
                        <img width="27" height="27" class="add" alt="add" src="<?php echo plugins_url( 'img/product_add.png' , __FILE__ ) ?>" />
                    </a>
                    <?php endif ?>
                    <p class="meta" style="width:'<?php echo $ps_object->get_image_width(); ?>px'" >
                        <a href="<?php echo $ps_object->get_permalink(); ?>"><?php echo $title ?></a>
                        <br />
                    <?php if($main_option['display_price'] && $main_option['display_type'] == 'product' ):?>
                        <em><?php echo $ps_object->get_price(); ?></em>
                    <?php endif ?>
                    </p>
                    <?php if( $main_option['display_type'] == 'product' ) : ?>
                    <form class="product_form" id="product_<?php echo $ps_object->get_post_id(); ?>" name="product_<?php echo $ps_object->get_post_id(); ?>" method="post" action="" enctype="multipart/form-data" >
                        <input type="hidden" name="wpsc_ajax_action" value="add_to_cart" />
                        <input type="hidden" name="product_id" value="<?php echo $ps_object->get_post_id(); ?>" />
                        <input type="hidden" name="key" value="<?php echo  wpsc_the_cart_item_key(); ?>" />
                        <input type="hidden" name="wpsc_update_quantity" value="true" />
                        <input type="hidden" name="product_image_width" value="<?php echo $ps_object->get_image_width(); ?>" />
                     </form>
                     <?php endif ?>
                </li>
            <?php endforeach ?>
            </ul>
            <div class="sliderContainer">
               <span class="btn-left"></span>
                <span class="start"></span>
                <span class="btn-right"></span>
                <span class="end"></span>
                <div class="slider">
                	<div class="ui-slider-handle"></div>
                </div>
            </div>
            </div><!-- sliderGallery group -->
        </div> <!-- end sliderWrap -->
        <?php
    }
}