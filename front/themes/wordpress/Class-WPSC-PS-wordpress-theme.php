<?php
Class Wpsc_ps_Wordpress_admin implements I_wpsc_ps_Admin {

    const SETTINGS_SECTION_WP    = 'product_slider_section_wp';
    private $option_list = array();
    private $option = array();

    public function __construct(){
        add_action('wpsc-ps-admin-page-form', array( &$this, 'admin_form'));
        add_filter('wpsc-ps-default-option',  array( &$this, 'set_default_options'));
        add_filter('wpsc_ps_add_theme_to_menu', array( &$this, 'add_to_menu'));
    }
    public function set_default_options( $defaults ){
        $defaults['wp_options'] = array(
                                    'visible' => 4,
                                    'scrolls' => 1,
                                    'image-width' => 96,
                                    'image-height'=> 96 );

        return $defaults;
    }
    public function add_to_menu( $theme_list ){
        // as the value add the name of the front end class for this theme
        $theme_list[] = array( 'value' => 'Wpsc_ps_Wordpress_theme'   , 'title' => __('WordPress','wpsc') );
        return $theme_list;
    }
    public function admin_form(){
       $this->option_list = array(
           'visible' => array(1,2,3,4,5,6,7,8,9,10),
           'scrolls' => array(1,2,3,4,5,6,7,8,9,10),
           'image-width' => 0,
           'image-height'=> 0 );

       $this->option = get_option( WPSC_PS_MAIN_OPTION );

       add_settings_section(   self::SETTINGS_SECTION_WP,
                               __( '', 'wpsc' ),
                               array( &$this, 'wp_html'),
                               Wpsc_ps_admin_page::SETTINGS_PAGE);
       ;
       add_settings_field( 'wp_field_visible',__( 'Visible', 'wpsc'), array( &$this, 'wp_field_visible'), Wpsc_ps_admin_page::SETTINGS_PAGE, self::SETTINGS_SECTION_WP);
       add_settings_field( 'wp_field_scroll_by',__( 'Scroll By', 'wpsc'), array( &$this, 'wp_field_scroll_by'), Wpsc_ps_admin_page::SETTINGS_PAGE, self::SETTINGS_SECTION_WP);
       add_settings_field( 'wp_field_image_width',__( 'Image Width', 'wpsc'), array( &$this, 'wp_field_image_width'), Wpsc_ps_admin_page::SETTINGS_PAGE, self::SETTINGS_SECTION_WP);
       add_settings_field( 'wp_field_image_height',__( 'Image Height', 'wpsc'), array( &$this, 'wp_field_image_height'), Wpsc_ps_admin_page::SETTINGS_PAGE, self::SETTINGS_SECTION_WP);
   }
   public function wp_html(){
       echo '<div class="misc-pub-section wp"></div>';
       echo '<h4 class="wp" >Wordpress Theme Settings</h4>';
   }
   public function wp_field_visible(){
       WPSC_Form_helper::drop_down_set(WPSC_PS_MAIN_OPTION . '[wp_options][visible]', $this->option_list['visible'], $this->option['wp_options']['visible']);
       echo '<p class="install-help" >Choose the number of products to be visible at one time. (10 max) </p>';
   }
   public function wp_field_scroll_by(){
       WPSC_Form_helper::drop_down_set(WPSC_PS_MAIN_OPTION . '[wp_options][scrolls]', $this->option_list['scrolls'], $this->option['wp_options']['scrolls']);
       echo '<p class="install-help" >Choose the number of products/items to scroll on 1 click</p>';
   }
   public function wp_field_image_width(){
       WPSC_Form_helper::number_input(WPSC_PS_MAIN_OPTION . '[wp_options][image-width]',$this->option['wp_options']['image-width'],3);
       echo '<p class="install-help" >The width of the images in pixels. Enter a value between 50 and 800</p>';
   }
   public function wp_field_image_height(){
       WPSC_Form_helper::number_input(WPSC_PS_MAIN_OPTION . '[wp_options][image-height]',$this->option['wp_options']['image-height'],3);
       echo '<p class="install-help" >The height of the images in pixels. Enter a value between 50 and 800</p>';
   }

   public function admin_save(){

   }


}
new Wpsc_ps_Wordpress_admin();




Class Wpsc_ps_Wordpress_theme implements I_wpsc_ps_Theme {



       public function load_css(){
        wp_enqueue_style('ps-wordpress', plugins_url('css/wpsc-ps-wordpress.css', __FILE__ ));
    }
    public function load_scripts(){

        wp_enqueue_script('jcarousellite', plugins_url( 'js/jCarouselLite.js' , __FILE__ ), array( 'jquery' ), '1.0.1' );
    	wp_enqueue_script('ps_wordpress', plugins_url( 'js/wpsc-ps-wordpress.js' , __FILE__ ), array( 'jquery', 'livequery','jcarousellite' ) );
    }
    public function get_theme_data(){

        $theme_data = array( 'theme'=> '', 'width' => 0, 'height' => 0);
        $master_option = get_option(WPSC_PS_MAIN_OPTION);

        $theme_data['theme'] = 'wp';
        $theme_data['width'] = $master_option['wp_options']['image-width'];
        $theme_data['height'] = $master_option['wp_options']['image-height'];


        return $theme_data;
    }
    public function render( $product_slider_data ){
        Wpsc_ps_Unquie_id::new_id();
        $id = Wpsc_ps_Unquie_id::get_id();
        $main_option = get_option(WPSC_PS_MAIN_OPTION);
        $ps_option = $main_option['wp_options'];
        // TODO in hidden inputs test name, was id?
        ?>
        <div class="wpsc-ps-wrap">
            <div id="gallery_slider_<?php echo $id; ?>" class="wpsc-ps-gallery_slider" >
                <img src="<?php echo plugins_url( 'img/drop_spinner.gif' , __FILE__ ) ?>" alt="Load Spinner" class="dragdrop_spinner_slider" />
                <input type="hidden" name="wpec_slider_visibles" class="wpec_slider_visibles"  value="<?php echo $ps_option['visible']; ?>" />
                <input type="hidden" name="wpec_slider_scrolls" class="wpec_slider_scrolls"  value="<?php echo $ps_option['scrolls']; ?>" />
               <a  id="wpec_ps_prev_<?php echo $id; ?>" href="#" title="<?php _e('Previous','wpsc'); ?>" class="wpec_ps_prev"><?php _e('Previous','wpsc'); ?></a>
             	<ul>

                <?php

                     foreach( $product_slider_data as $ps_object):

                        if ( strlen($ps_object->get_title()) > 30 )
                 		    $title  = substr( $ps_object->get_title(), 0, 10) . '...';
                        else
                            $title = $ps_object->get_title();

                     ?>
                     <li>
                         <p class="meta">
                             <a href="<?php echo $ps_object->get_permalink(); ?>" ><?php echo $title; ?></a>
                             <?php if($main_option['display_price'] && $main_option['display_type'] == 'product' ):?>
                                <em><?php echo $ps_object->get_price(); ?></em>
                             <?php endif ?>
                         </p>
                         <a href="<?php echo $ps_object->get_permalink(); ?>" >
                             <img class="item product_image" id="product_image_<?php echo $ps_object->get_post_id(); ?>" alt="<?php echo $title ?>" title="<?php echo $title ?>" src="<?php echo $ps_object->get_image_url(); ?>" width="<?php echo $ps_object->get_image_width(); ?>" height="<?php echo $ps_object->get_image_height(); ?>" />
                         </a>
                         <?php if( $main_option['display_type'] == 'product' ) : ?>
                         <a href="javascript:void(0);" class="add_product">
                            <img width="27" height="27" class="add" alt="add" src="<?php echo plugins_url( 'img/product_add.png' , __FILE__ ) ?>" />
                         </a>
                         <form class="product_form slider" id="product_<?php echo $ps_object->get_post_id(); ?>" name="product_<?php echo $ps_object->get_post_id(); ?>" method="post" action="" enctype="multipart/form-data" >
                            <input type="hidden" name="wpsc_ajax_action" value="add_to_cart" />
                            <input type="hidden" name="product_id" value="<?php echo $ps_object->get_post_id(); ?>" />
                            <input type="hidden" name="key" value="<?php echo  wpsc_the_cart_item_key(); ?>" />
                            <input type="hidden" name="wpsc_update_quantity" value="true" />
                         </form>
                         <?php endif; ?>
                     </li>
                <?php endforeach ?>
                </ul>
                <a href="#" id="wpec_ps_next_<?php echo $id; ?>" title="<?php _e('Next','wpsc'); ?>" class="wpec_ps_next"><?php _e('Next','wpsc'); ?></a>
            </div> <!-- close gallery_slider -->
        </div> <!-- close wpsc-ps-wrap -->
        <?php
    }
}
